import unittest, json, sys
sys.path.append('')
import MartianWeather as mw

class TestWeather(unittest.TestCase):
    def setUp(self):
        with open('tests/test_data/nasa2.json', 'r') as f:
            self.data = json.loads(f.read())

        self.sols = mw.get_sols(self.data)

    def test_sol(self):
        for sol in self.sols:
            self.assertIs(type(sol.key), str)
            self.assertIs(type(sol.season), str)
            self.assertIs(type(sol.first_utc), str)
            self.assertIs(type(sol.last_utc), str)
            self.assertIs(type(sol.temp), mw.Temp)
            self.assertIs(type(sol.pressure), mw.Pressure)
            self.assertIs(type(sol.wind_speed), mw.WindSpeed)
            self.assertIs(type(sol.wind_dir), mw.WindCompass)

    def test_temp(self):
        for sol in self.sols:
            self.assertIs(type(sol.temp.valid), bool)
            if sol.temp.valid:
                self.assertIs(type(sol.temp.avg), float)
                self.assertIs(type(sol.temp.min), float)
                self.assertIs(type(sol.temp.max), float)
                self.assertIs(type(sol.temp.count), int)
            elif not sol.temp.valid:
                self.assertIsNone(sol.temp.avg)
                self.assertIsNone(sol.temp.min)
                self.assertIsNone(sol.temp.max)
                self.assertIsNone(sol.temp.count)

    def test_pressure(self):
        for sol in self.sols:
            self.assertIs(type(sol.pressure.valid), bool)
            if sol.pressure.valid:
                self.assertIs(type(sol.pressure.avg), float)
                self.assertIs(type(sol.pressure.min), float)
                self.assertIs(type(sol.pressure.max), float)
                self.assertIs(type(sol.pressure.count), int)
            elif not sol.pressure.valid:
                self.assertIsNone(sol.pressure.avg)
                self.assertIsNone(sol.pressure.min)
                self.assertIsNone(sol.pressure.max)
                self.assertIsNone(sol.pressure.count)

    def test_wind_speed(self):
        for sol in self.sols:
            self.assertIs(type(sol.wind_speed.valid), bool)
            if sol.wind_speed.valid:
                self.assertIs(type(sol.wind_speed.avg), float)
                self.assertIs(type(sol.wind_speed.min), float)
                self.assertIs(type(sol.wind_speed.max), float)
                self.assertIs(type(sol.wind_speed.count), int)
            elif not sol.wind_speed.valid:
                self.assertIsNone(sol.wind_speed.avg)
                self.assertIsNone(sol.wind_speed.min)
                self.assertIsNone(sol.wind_speed.max)
                self.assertIsNone(sol.wind_speed.count)

    def test_wind_dir(self):
        for sol in self.sols:
            self.assertIs(type(sol.wind_dir.valid), bool)
            self.assertIs(type(sol.wind_dir.common.count), int)
            for sensor in sol.wind_dir.sensors:
                self.assertIs(type(sensor), mw.WindCompass.CompassPoint)
                self.assertIs(type(sensor.count),  int)
                self.assertIs(type(sensor.vector), tuple)


if __name__ == '__main__':
    unittest.main()
