import unittest, sys
sys.path.append('')
import MartianWeather as mw

class TestWeather(unittest.TestCase):
    def setUp(self):
        key = 'DEMO_KEY'
        self.api = mw.Api(key)

    def test_api(self):
        self.assertIs(type(self.api.data), dict)
        self.assertIs(type(self.api.json), str)
        self.assertIs(type(self.api.available_sols), list)
        self.assertIs(type(self.api.status), int)
        self.assertIs(type(self.api.hours_required), int)
        self.assertIs(type(self.api.validity_checks), list)
        self.assertIs(type(self.api.requests_limit), int)
        self.assertIs(type(self.api.requests_remaining), int)


if __name__ == '__main__':
    unittest.main()
