# MartianWeather

MartianWeather is a python package that makes it easy to connect to the [Insight: Mars Weather Service API](https://mars.nasa.gov/insight/weather/). The package provides functions for accessing measurements of temperature, pressure and wind taken by the Insight lander on the surface of mars at Elysium Planitia.

### Installation

MartianWeather can be installed with the python package manager

``` 
pip3 install MartianWeather 
```

### Usage

For documentation on how to use MartianWeather please visit the [wiki](https://gitlab.com/wilmlar/martianweather/-/wikis/home) where you can find some simple [example scripts](https://gitlab.com/wilmlar/martianweather/-/wikis/Examples) and a full [reference](https://gitlab.com/wilmlar/martianweather/-/wikis/Reference) page.